//
//  PokemonDetailViewController.swift
//  pokedex
//
//  Created by Dennis Veintimilla on 20/6/18.
//  Copyright © 2018 Dennis Veintimilla. All rights reserved.
//

import UIKit

class PokemonDetailViewController: UIViewController {
    
    @IBOutlet weak var pokemonNameLabel: UILabel!
    @IBOutlet weak var pokemonWeightLabel: UILabel!
    @IBOutlet weak var pokemonHeightLabel: UILabel!
    @IBOutlet weak var pokemonImageView: UIImageView!
    var pokemon:Pokemon!
    var selectedRow: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        pokemonNameLabel.text? = pokemon.name
        pokemonHeightLabel.text = "\(pokemon.height)"
        pokemonWeightLabel.text = "\(pokemon.weight)"
        
        
        let bm = Network()
        bm.getPokemonImage(url: pokemon.sprites.defaultSprite, completionHandler: { (imageR) in
            DispatchQueue.main.async {
                self.pokemonImageView.image = imageR
            }
            
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
